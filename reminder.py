from dice_roller import FormatError
from parsedatetime import Calendar
from time import time, mktime
import re

cal = Calendar()

def remind(message):
    try:
        t, r = re.match("(\([^\n)]*\))(.*)", message, flags=re.DOTALL).groups()
    except AttributeError:
        raise FormatError("Couldn't parse the reminder!")
    
    t, s = cal.parse(t)
    t = mktime(t) - time()

    if not s or t < 0:
        raise FormatError("Time specification was unreasonable!")

    return t, r.strip()


if __name__ == "__main__":
    t, r = remind("(in 10 minutes) I need to write shit")

    print(t)
    print(r)
