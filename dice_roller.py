from collections import deque
from functools import wraps
import random
import signal
import errno
import re
import os

class TimeoutError(Exception):
     def __init__(self, message):
         self.message = message
     def __str__(self):
         return repr(self.message)


class FormatError(Exception):
     def __init__(self, message):
         self.message = message
     def __str__(self):
         return repr(self.message)


def timeout(seconds=10, error_message="Timed out!"):
    def decorator(func):
        if os.name != "nt":
            def _handle_timeout(signum, frame):
                raise TimeoutError(error_message)

            def wrapper(*args, **kwargs):
                signal.signal(signal.SIGALRM, _handle_timeout)
                signal.alarm(seconds)
                try:
                    result = func(*args, **kwargs)
                finally:
                    signal.alarm(0)
                return result

            return wraps(func)(wrapper)
        return func

    return decorator


class ArithmeticRoller(object):
    def __init__(self, max_rolls=300):
        self.max_rolls = max_rolls

        self.math_prec = {
            "(" : 1,
            "+" : 2, "-"  : 2,
            "*" : 3, "/"  : 3,
            "^" : 4, "**" : 4
        }

        self.math_ops = {
            "+" : lambda a, b: a+b, "-"  : lambda a, b: a-b,
            "*" : lambda a, b: a*b, "/"  : lambda a, b: a/b,
                                    "**" : lambda a, b: a**b
        }

        # self.dice_mod = {"r":self.r, "x":self.x, "h":self.h}

        # Recognises dice, does not match simple numbers or decimals
        dice_re_str = "[0-9]+|".join(["r", "x", "h", "e", "s"])
        dice_re_str = "([0-9]+d[0-9]+(?:{}[0-9]+)*)(?=\W)".format(dice_re_str)
        self.dice_re = re.compile(dice_re_str)

        # Validates mathematical operations and numbers only
        self.val_re = re.compile("[()+\-*/^0-9. ;]+")

        self.math_split_re = re.compile("(\(|\)|\+|-|\*+|/|\^)")


    @timeout(3, "Evaluation timed out. Please don't try to break my bot!")
    def parse(self, string, hod=False):
        # Seperate out the comment
        dice_str, _, comment = string.partition("#")

        # Split
        dice_split = self.dice_re.split(dice_str.lower().strip() + " ")

        # Validate the non-dice bits
        for n, i in enumerate(dice_split[::2]):
            if i and not self.val_re.fullmatch(i):
                err = "Invalid format at `%s`" % "".join(dice_split[:2*n+1])
                raise FormatError(err)

        dice_full_rolls = []

        # Roll the dice, expand them in place
        for n, die_str in enumerate(dice_split[1::2]):
            full, trun = self.roll_dice(die_str, hod)
            dice_full_rolls.append(full)
            dice_split[1+n*2] = trun

        # Create dice display string
        disp_str = " ".join(dice_full_rolls)

        # Remove spaces from parse string
        math_str = "".join(dice_split).replace(" ", "")

        # Postfix the math string
        math_str = self.post_fix(math_str)

        # Process the math string
        math_str = self.post_math(math_str)

        # Create math display string
        math_str = "; ".join(str(i) for i in math_str)

        return disp_str, math_str


    def roll_dice(self, die_str, hod=False):
        # Parse the modifiers
        modifiers = re.split("(\D+)", die_str)

        modifiers = self.parse_modifiers(modifiers, hod)

        # Roll it!
        res, res_trn = self.roll(modifiers)

        return self.format_helper(res, res_trn)


    def parse_modifiers(self, mods, hod = False):
        out = {"n" : int(mods[0]), "f" : int(hod)}

        for m, v in zip(*[iter(mods[1:])]*2):
            try:
                out[m].append(int(v))
            except KeyError:
                out[m] = [int(v)]

        return out


    def roll(self, mods):
        # Roll Nd[...]
        res = [self.single_roll(mods) for n in range(mods["n"])]

        # Handle highest (h)
        try:
            res_trn = sorted(res, key=lambda x: sum(x))[-mods['h'][0]:]
        except KeyError:
            res_trn = res

        # Handle success (s)
        try:
            res_trn = [[sum(j >= mods['s'][0] for i in res for j in i)]]
        except KeyError:
            pass

        try:
            if sum(j for i in res_trn for j in i) < mods["f"]:
                res, res_trn = self.roll(mods)
        except KeyError:
            pass

        return res, res_trn


    def single_roll(self, mods):
        res = deque()

        for _ in range(self.max_rolls):
            # make a roll
            res.append(random.randint(1, mods["d"][0]))

            # Handle rerolls (r)
            try:
                if res[-1] in mods['r']:
                    res.pop()
                    continue
            except KeyError:
                pass

            # Handle emphasis(e)
            try:
                if res[-1] in mods['e']:
                    res.pop()
                    del mods['e']
                    continue
            except KeyError:
                pass

            # Handle explodes (x)
            try:
                if res[-1] in mods['x']:
                    continue
            except KeyError:
                pass

            # Will only reach here if all conditions satisfied
            break

        else:
            # For infinite rerolls without success
            if not res:
                res.append(0)


        return res


    def format_helper(self, res, res_trn):
        return self.format_dice(res, par=True), self.format_dice(res_trn)


    def format_dice(self, res, par=False):
        return ["({})", "<{}>"][par].format(
            "+".join(
                "({})".format(
                    "+".join(str(i) for i in j)
                ) 
                if len(j)>1 
                else str(j[0]) 
                for j in res
            )
        )


    def post_fix(self, math_str):
        math_strs = math_str.replace("^", "**").split(";")
        postfixed = []

        for m_str in math_strs:
            m_str = [i for i in self.math_split_re.split(m_str) if i]
            stack, out = deque(), deque()
            t_prec = 0

            for t in m_str:
                try:
                    c_prec = self.math_prec[t]

                except KeyError:
                    # Not an operator, so integer, float, or )
                    if t == ")":
                        while len(stack):
                            curr = stack.pop()
                            if curr == "(":
                                break
                            else:
                                out.append(curr)
                        else:
                            raise FormatError("Mismatched brackets!")

                    elif "." in t:
                        try:
                            out.append(float(t))
                        except ValueError:
                            raise FormatError("Incorrect formatting!")

                    else:
                        out.append(int(t))

                else:
                    # Is (
                    if t == "(":
                        stack.append(t)
                        continue

                    # Is an operator with precedence c_prec
                    t_prec = self.math_prec[stack[-1]] if len(stack) else 0

                    while c_prec <= t_prec or (t == "**" and c_prec < t_prec):
                        out.append(stack.pop())
                        t_prec = self.math_prec[stack[-1]] if len(stack) else 0

                    stack.append(t)
                    t_prec = self.math_prec[stack[-1]] if len(stack) else 0

            # End of string, clean up stack
            while len(stack):
                curr = stack.pop()
                if curr == "(":
                    raise FormatError("Mismatched brackets!")
                else:
                    out.append(curr)
                
            postfixed.append(out)

        return postfixed


    def post_math(self, math_strs):
        out = []
        for m_deq in math_strs:
            stack = deque()

            for t in m_deq:
                try:
                    op = self.math_ops[t]
                except KeyError:
                    stack.append(t)
                else:
                    b, a = stack.pop(), stack.pop()
                    try:
                        stack.append(op(a, b))
                    except ZeroDivisionError:
                        raise FormatError("You divided by zero. Don't do that!")

            try:
                out.append(stack[0])
            except IndexError:
                pass

        return out



def choice(string):
    try:
        no, f, hold = string.split(" ", 2)
    except ValueError:
        no = 1
    else:
        if f == "from":
            try:
                no = int(no)
                string = hold
            except TypeError:
                no = 1
        else:
            no = 1

    choices = [i.strip() for i in string.split(",")]

    return ", ".join(random.sample(choices, no))


if __name__ == "__main__":
    tests = [
        "10d10x9x10x8x7r1r2r3h3",
        "9d10x9x10r1r2x8r3r4h5 + (2d6x6e1 + 3+1d6)^2*8d5h4- 1.3 # comment!",
        "5d10x5x6x7x8x9x10s4",
        "6d10h3;",
        "10d1r1",
        ".*"
    ]

    roller = ArithmeticRoller()

    for i in tests:
        try:
            print(roller.parse(i))
        except FormatError as e:
            print(e)

    print(roller.parse("2d6x6 + 8", 15))
