from aiohttp.web import *
from discord.client import *

import traceback
import asyncio
import os

class Client(Client):
    # Modified client that also serves http requests in the same event loop
    # Kludged together from the discord Client and the aiohttp Application
    
    def __init__(self, *args, loop=None, **options):
        super(Client, self).__init__(*args, **options)
        self.app = Application(loop=self.loop)


    def run(self, *args, host='0.0.0.0', port=None,
            shutdown_timeout=60.0, ssl_context=None,
            print=print, **kwargs):

        if port is None:
            if not ssl_context:
                port = 8080
            else:
                port = 8443

        handler = self.app.make_handler(
            access_log_format='%{x-forwarded-for}i "%r" %s %b "%{User-Agent}i"'
        )
        srv = self.loop.run_until_complete(
            self.loop.create_server(handler, host, port, ssl=ssl_context)
        )

        scheme = 'https' if ssl_context else 'http'

        try:
            self.loop.run_until_complete(self.start(*args, **kwargs))

        except KeyboardInterrupt:
            self.loop.run_until_complete(self.logout())
            pending = asyncio.Task.all_tasks()
            gathered = asyncio.gather(*pending)

            srv.close()
            self.loop.run_until_complete(srv.wait_closed())
            self.loop.run_until_complete(self.app.shutdown())
            self.loop.run_until_complete(
                handler.finish_connections(shutdown_timeout)
            )
            self.loop.run_until_complete(self.app.cleanup())

            try:
                gathered.cancel()
                self.loop.run_until_complete(gathered)

                # we want to retrieve any exceptions to make sure that
                # they don't nag us about it being un-retrieved.
                gathered.exception()
            except:
                pass
        finally:
            self.loop.close()



    @asyncio.coroutine
    async def on_ready(self, *args, **kwargs):
        n, i = self.user.name, self.user.id
        log.info('==========\nConnected!\nUsername: {}\nID: {}'.format(n, i))

        self.me = (await (self.application_info())).owner


    @asyncio.coroutine
    async def on_error(self, event_method, *args, **kwargs):
        err = traceback.format_exc().replace('\n', '\n    ')
        log.warning("Ignoring error\n{}".format(err))
        
        try:
            origin = args[0]
            msg = "```py\n{}\n``` at <#{}>\n{}: {}".format(
                err,
                origin.channel.id,
                origin.author.name,
                origin.content
            )
        except IndexError:
            msg = "```py\n{}\n```".format(err)

        await self.send_message(self.me, msg)
