import discord_mod as discord

from logging.handlers import RotatingFileHandler
from contextlib import redirect_stdout
from dice_roller import FormatError
from aiohttp import web
from time import time

import dice_roller as dr
import traceback
import reminder
import asyncio
import logging
import sqlite3
import inspect
import random
import zalgo
import os
import io

                                   #########
#################################### Setup ####################################
                                   #########
# Get environmental variables
log_dir = os.environ.get("OPENSHIFT_LOG_DIR")
data_dir = os.environ.get("OPENSHIFT_DATA_DIR")
local_host = os.environ.get("OPENSHIFT_DIY_IP")
local_port = os.environ.get("OPENSHIFT_DIY_PORT")

# Set up client
client = discord.Client()

# Set up loggers
gen_for = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')
acc_for = logging.Formatter(
    '%a %l %u %t "%r" %s %b "%{x-forwarded-for}i" "%{User-Agent}i"'
)

# Log files
gen_log = os.sep.join((log_dir, 'general.log'))
acc_log = os.sep.join((log_dir, 'access.log'))

# File handlers
gen_log = RotatingFileHandler(
    filename = gen_log, encoding='utf-8', maxBytes=1048576, backupCount=1
)
acc_log = RotatingFileHandler(
    filename = acc_log, encoding='utf-8', maxBytes=1048576, backupCount=1
)

gen_log.setFormatter(gen_for)
acc_log.setFormatter(gen_for)

logger = logging.getLogger('aiohttp.access')
logger.addHandler(acc_log)
logger.setLevel(logging.DEBUG)

# logger = logging.getLogger('aiohttp.web')
# logger.addHandler(gen_log)
# logger.setLevel(logging.DEBUG)

logger = logging.getLogger('discord')
logger.addHandler(gen_log)
logger.setLevel(logging.DEBUG)

# SQL database setup
conn = sqlite3.connect(os.sep.join((data_dir, 'db.db')))
c = conn.cursor()
c.execute(
    "CREATE TABLE IF NOT EXISTS "
    "rolls ("
        "id INTEGER PRIMARY KEY, "
        "dt default CURRENT_TIMESTAMP, "
        "user TEXT, "
        "roll TEXT, "
        "resu TEXT, "
        "disp TEXT, "
        "comm TEXT"
    ")"
)
conn.commit()

class REPL:
    def __init__(self, client):
        self.client = client
        self.sessions = set()

    def cleanup_code(self, content):
        """Automatically removes code blocks from the code."""
        # remove ```py\n```
        content = content.replace("```py", "").replace("```", "")
        content = "\n".join(
            line 
            for line in content.splitlines()
            if line.strip()
        )

        # remove `foo`
        return content.strip('` \n')


    def get_syntax_error(self, e):
        return '```py\n{0.text}{1:>{0.offset}}\n{2}: {0}```'.format(e, '^', type(e).__name__)


    async def repl(self, msg):
        variables = {
            'client': self.client,
            'message': msg,
            'server': msg.server,
            'channel': msg.channel,
            'author': msg.author,
            'last': None,
        }

        if msg.channel.id in self.sessions:
            await self.client.send_message(
                msg.channel,
                'Already running a REPL session in this channel. Exit it with `quit`.'
            )
            return

        self.sessions.add(msg.channel.id)
        await self.client.send_message(
            msg.channel,
            'Enter code to execute or evaluate. `exit()` or `quit` to exit.'
        )
        while True:
            response = await self.client.wait_for_message(author=msg.author, channel=msg.channel,
                                                       check=lambda m: m.content.startswith('`'))

            cleaned = self.cleanup_code(response.content)

            if cleaned in ('quit', 'exit', 'exit()'):
                await self.client.send_message(
                    msg.channel,
                    'Exiting.'
                )
                self.sessions.remove(msg.channel.id)
                return

            executor = exec
            if cleaned.count('\n') == 0:
                # single statement, potentially 'eval'
                try:
                    code = compile(cleaned, '<repl session>', 'eval')
                except SyntaxError:
                    pass
                else:
                    executor = eval

            if executor is exec:
                try:
                    code = compile(cleaned, '<repl session>', 'exec')
                except SyntaxError as e:
                    await self.client.send_message(
                        msg.channel,
                        self.get_syntax_error(e)
                    )
                    continue

            variables['message'] = response

            fmt = None
            stdout = io.StringIO()

            try:
                with redirect_stdout(stdout):
                    result = executor(code, variables)
                    if inspect.isawaitable(result):
                        result = await result
                    else:
                        try:
                            if not inspect.isawaitable(result[0]):
                                raise TypeError
                        except TypeError:
                            pass
                        else:
                            out = []
                            for res in result:
                                r = await res
                                out.append(r)
                            result = out

            except Exception as e:
                value = stdout.getvalue()
                fmt = '```py\n{}{}\n```'.format(value, traceback.format_exc())

            else:
                value = stdout.getvalue()

                if result is not None:
                    fmt = '```py\n{}{}\n```'.format(value, result)
                    variables['last'] = result

                elif value:
                    fmt = '```py\n{}\n```'.format(value)

            try:
                if fmt is not None:
                    if len(fmt) > 2000:
                        await self.client.send_message(msg.channel, 'Content too big to be printed.')
                    else:
                        await self.client.send_message(msg.channel, fmt)
            except discord.Forbidden:
                pass
            except discord.HTTPException as e:
                await self.client.send_message(msg.channel, 'Unexpected error: `{}`'.format(e))


class Responder:
    def __init__(self, c, conn):
        self.dispatch = {
            "/roll"      : self.roll,
            "/choose"    : self.choose,
            "/zalgo"     : self.zalgo,
            "/help"      : self.help,
            "/reminder"  : self.reminder,
            "/hid"       : self.hid,
            "/pat"       : self.pat,
            "/pats"      : self.pat,
            "/headpat"   : self.pat,
            "/headpats"  : self.pat,
            "/doka"      : self.doka,
            "/smug"      : self.smug,
            "/mado"      : self.mado,
            "/because"   : self.because,
            "/koko"      : self.koko,
            "/mura"      : self.koko,
            "/kyoko"     : self.koko,
            "/kyouko"    : self.koko,
            "/sayaka"    : self.ugo,
            "/saka"      : self.ugo,
            "/seyiku"    : self.ugo,
            "/ugo"       : self.saka,
            "/ugolino"   : self.saka,
            "/hairflips" : self.hairflips,
            "/repl"      : self.repl
        }

        # Help messages
        self.help_responses = {
            "nil" : "\n`/roll [dice format] # Comments proceeding the hash key."
                "`\n    Rolls dice according to the formatting given.\n\n"
                "`/choose [A], [B], [C]...`\n    Chooses between items [A], [B],"
                " [C]...\n\n`"
                "/zalgo [text]`\n    Added against my better judgement. "
                "Zalgoifies text, deletes after 30 seconds.\n\n"
                "`/reminder (time) [Reminder text]`\n    Sends a message "
                "containing the reminder text after (time).\n\n"
                "Additional information about commands may be found by typing "
                "/help followed by the command in question, eg. `/help /roll"
                "`\n\n`/pat [patee]`\nHeadpats the patee.\n\nType `/help "
                "/formatting` for help about formatting text" ,

            "roll" : "The dice format must first consist of the dice specificat"
                "ion, in the form `XdY`. Additional specifiers may be:\n"
                "`xY`, (explode) roll the dice again, adding the result, on a "
                "roll of Y\n"
                "`rY`, (reroll) roll the dice again, discarding the previous "
                "roll, on a roll of Y\n"
                "`eY`, (emphasis) like reroll, but will only roll and discard "
                "once\n"
                "`hY`, (highest) keep the higest Y dice (including explodes)\n"
                "`sY`, (success) count how many dice exceed a certain value. "
                "This takes priority over keep highest\n"
                "`;`, (multiple rolls) semicolons may be used to seperate "
                "multiple rolls \n"
                "Rolls may be verified by accessing the ID given by the roll, "
                "http://rollerbot-sqtheta.rhcloud.com/rolls/ID\n" ,

            "choose" : "You can also type `/choose N from [list]` to choose wit"
                "hout replacement N items from the list of choices.",

            "formatting" : "\*asterisks\* or \_underscores\_ for *italics*\n"
                "\*\*double asterisks\*\* for **bold**\n"
                "\_\_double underscore\_\_ for __underline__\n"
                "\~\~double tilde\~\~ for ~~strikethrough~~\n"
                "\`backticks\` for `code`\n"
                "\`\`\`triple backticks\`\`\` for ```multi\n    line\n"
                "     code```"
                "and \\\\ backslashes to \\\\\\_escape formatting\\\\\\_, "
                "though this is weird on mobile.",

            "zalgo" : "Added against my better judgement. Zalgoifies text. You "
            "can type `/zalgo int N text` to set the intensity; default is 5",

            "reminder" : "The time specification may be in any simple format, l"
               "ike 'five minutes time' or '5 mins' or '10 hours' and so forth."
        }

        self.help_responses['format'] = self.help_responses['formatting']
        
        self.repo_dir = os.environ.get("OPENSHIFT_REPO_DIR")

        self.c, self.conn = c, conn

        self.roller = dr.ArithmeticRoller()

        self.hid = None, None


    def roll(self, message):
        # Handle comment
        roll, _, comment = message.content[6:].partition("#")

        if "{}{}".format(message.author.id, comment.strip()) == self.hid[0]:
            self.hod = self.hid[1]
        else:
            self.hod = False
        
        comment = " # " + comment.strip() if comment else comment

        # Do the rolls!
        try:
            disp_str, result = self.roller.parse(message.content[6:], self.hod)

        except (FormatError, dr.TimeoutError) as e:
            return e.message, 0

        # Write to database
        self.c.execute(
            "insert into rolls (user,roll,resu,disp,comm) values (?,?,?,?,?)", 
            (message.author.display_name, roll, result, disp_str, comment)
        )
        self.conn.commit()

        # I blame Nat
        if "69" in result:
            if comment:
                comment += " (L-leeewd!)"
            else:
                comment = "# L-leeewd!"

        if self.hod:
            self.hid, self.hod = ("", ""), False

        # Format results
        result = "**{}** rolled {} = **{}** {} {} [ID: {}]".format(
            message.author.display_name, 
            roll,
            result,
            disp_str,
            comment,
            c.lastrowid
        )

        # Log the results
        logger.info(result)

        return result, 0


    def choose(self, message):
        return dr.choice(message.content[8:]), 0


    def zalgo(self, message):
        return zalgo.parse(message.content[7:]), 2


    def help(self, message):
        try:
            param = message.content.split()[1].strip("/")
            response = self.help_responses[param]

        except (KeyError, IndexError):
            response = self.help_responses["nil"]

        return response, 0


    def reminder(self, message):
        try:
            t, r = reminder.remind(message.content[10:])
        except FormatError as e:
            return e.message, 0

        return (r, t), 3


    def hid(self, message):
        global me
        if message.author == me:
            n, _, h = message.content[5:].partition(" ")
            self.hid = "{}{}".format(me.id, h.strip()), n

            return self.hid, 0


    def pat(self, message):
        return "( ＾ᴗ＾)ノ” *headpats {}*".format(
            message.content.split(" ", 1)[1]
        ), 0


    def doka(self, message):
        return os.sep.join([self.repo_dir, "doak.png"]), 1


    def smug(self, message):
        return os.sep.join([self.repo_dir, "smug.gif"]), 1


    def mado(self, message):
        return os.sep.join([self.repo_dir, "mado.png"]), 1


    def hairflips(self, message):
        return os.sep.join([self.repo_dir, "hairflips.gif"]), 1


    def because(self, message):
        return os.sep.join([self.repo_dir, "because.jpg"]), 1


    def koko(self, message):
        return os.sep.join([self.repo_dir, "koko.gif"]), 1


    def saka(self, message):
        return os.sep.join([self.repo_dir, "sayaka.jpg"]), 1


    def ugo(self, message):
        return os.sep.join([self.repo_dir, "ugolino.png"]), 1


    def repl(self, message):
        if client.me == message.author:
            return message, 4


    # def evaluate(self, message):
    #     if client.me == message.author:

    #         command = message.content.split(" ", 1)[1]
    #         command = command.replace("```py", "").replace("```", "")
    #         command = "\n".join(
    #             line 
    #             for line in command.splitlines()
    #             if line.strip()
    #         )

    #         try:
    #             global ret
    #             del ret
    #         except NameError:
    #             pass

    #         command = "global ret\n" + command

    #         try:
    #             exec(command)
    #             return ret, 0

    #         except NameError:
    #             return "No return value was defined, but code executed.", 0

    #         except Exception:
    #             err = traceback.format_exc().replace('\n', '\n    ')
    #             return "```py\n{}```".format(err), 0


    def message_handler(self, message):
        if "no pat" in message.content:
            if not random.randint(0, 100):
                return "yes pats", 0

        if not message.content.startswith("/"):
            return

        cmd = message.content.split()[0]

        try:
            return self.dispatch[cmd](message)
        except KeyError:
            return


                                   ##########
#################################### Client ####################################
                                   ##########


responder = Responder(c, conn)
repl = REPL(client)

@client.event
async def on_message(message):
    global me
    try:
        me
    except NameError:
        me = (await (client.application_info())).owner

    try:
        response, m = responder.message_handler(message)
    except TypeError:
        return

    if m == 0:
        await client.send_message(message.channel, response)

    elif m == 1:
        await client.send_file(message.channel, response)

    elif m == 2:
        message = await client.send_message(message.channel, response)
        await asyncio.sleep(20)
        await client.delete_message(message)

    elif m == 3:
        r, t = response
        await client.send_message(message.author, "Reminder logged!")
        await asyncio.sleep(t)
        await client.send_message(message.author, r)

    elif m == 4:
        await repl.repl(response)




                              ###################
############################### TTS Politics OK ###############################
                              ###################

@client.event
async def on_member_join(member):
    if member.server.id == "192517230515322881":
        for role in member.server.roles:
            if role.name == "politics-ok":
                await client.add_roles(member, role)
                return


                              ##################
############################### Server routing #################################
                              ##################

async def hello(request):
    return web.Response(body=b"Hello, world")


async def lookup_roll(request):
    idee = request.match_info["id"]
    try:
        c.execute("select * from rolls where rowid=?", (idee,))
        form = "{} {} rolled {} = {} {} {}"
        resp = form.format(*c.fetchone()[1:]).encode("utf-8")

    except (TypeError, sqlite3.Error):
        resp = "No such ID {} found!".format(idee).encode("utf-8")

    return web.Response(body=resp)


client.app.router.add_route('GET', '/rolls/{id}', lookup_roll)
client.app.router.add_route('GET', '/', hello)




client.run('MTg5NjI4MTY5Mjk5NDI3MzI4.CjgJiA.2ptgjzAv_ChYJesA3OZyfzpPcW8'
    , host=local_host, port=local_port
)
conn.close()

# https://discordapp.com/oauth2/authorize?client_id=189628068569022464&scope=bot&permissions=11264
# git update-index --chmod=+x .openshift\action_hooks\*
# rhc ssh --ssh D:\SublimeGitKivy\bin\ssh.exe
# killall python3;export PATH=$OPENSHIFT_DATA_DIR/usr/bin:$PATH;python3 $OPENSHIFT_REPO_DIR/main.py
# 36393E